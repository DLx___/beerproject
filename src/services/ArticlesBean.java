package services;

import application.Main;
import dao.DAOFactory;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import job.*;

import java.util.ArrayList;

public class ArticlesBean
{
    private static final String SHOW_ALL = "Afficher tous";
    private ObservableList<Continent> continents;
    private ObservableList<Continent> continentsFiltrer;
    private ObservableList<Pays> pays;
    private ObservableList<Pays> paysFiltrer;
    private ObservableList<Fabricant> fabricants;
    private ObservableList<Fabricant> fabricantsFiltrer;
    private ObservableList<Marque> marques;
    private ObservableList<Marque> marquesFiltrer;
    private ObservableList<Couleur> couleurs;
    private ObservableList<Couleur> couleursFiltrer;
    private ObservableList<TypeBiere> types;
    private ObservableList<TypeBiere> typesFiltrer;
    private ObservableList<Article> articles;
    private ObservableList<Article> listArticlesFiltres;
    private ObservableList<Article> listArticlesVendus;
    private Vente vente;
    private boolean saved;
    private Main main;
    private Article articleSelected;
    private ArticlesSearch articlesSearch;

    public ArticlesBean()
    {
        continents = FXCollections.observableArrayList(DAOFactory.getContinentDAO().getAll());
        pays = FXCollections.observableArrayList(DAOFactory.getPaysDAO().getAll());
        fabricants = FXCollections.observableArrayList(DAOFactory.getFabricantDAO().getAll());
        marques = FXCollections.observableArrayList(DAOFactory.getMarqueDAO().getAll());
        couleurs = FXCollections.observableArrayList(DAOFactory.getCouleurDAO().getAll());
        types = FXCollections.observableArrayList(DAOFactory.getTypeDAO().getAll());
        articles = FXCollections.observableArrayList(DAOFactory.getArticleDAO().getAll());
        listArticlesVendus = FXCollections.observableArrayList(DAOFactory.getArticleDAO().getAll());

        continentsFiltrer = FXCollections.observableArrayList(new ArrayList<>());
        paysFiltrer = FXCollections.observableArrayList(new ArrayList<>());
        fabricantsFiltrer = FXCollections.observableArrayList(new ArrayList<>());
        marquesFiltrer = FXCollections.observableArrayList(new ArrayList<>());
        couleursFiltrer = FXCollections.observableArrayList(new ArrayList<>());
        typesFiltrer = FXCollections.observableArrayList(new ArrayList<>());


        continentsFiltrer.addAll(continents);
        continentsFiltrer.add(0, new Continent(0, SHOW_ALL));
        paysFiltrer.addAll(pays);
        paysFiltrer.add(0, new Pays(0, SHOW_ALL));
        fabricantsFiltrer.addAll(fabricants);
        fabricantsFiltrer.add(0, new Fabricant(0, SHOW_ALL));
        marquesFiltrer.addAll(marques);
        marquesFiltrer.add(0, new Marque(0, SHOW_ALL));
        couleursFiltrer.addAll(couleurs);
        couleursFiltrer.add(0, new Couleur(0, SHOW_ALL));
        typesFiltrer.addAll(types);
        typesFiltrer.add(0, new TypeBiere(0, SHOW_ALL));


        this.articleSelected = new Article(0, null, 0, 0, null);
        articlesSearch = new ArticlesSearch();

        vente = new Vente(null, 0);
    }

    public Article getArticleSelected()
    {
        return articleSelected;
    }

    public void setArticleSelected(Article articleSelected)
    {
        this.articleSelected = articleSelected;
    }


    public void setArticlesSearch(String libelle, String volume, Double titrageMin, Double titrageMax, Marque marque, Couleur couleur, Fabricant fabricant, TypeBiere type, Pays pays, Continent continent)
    {
        articlesSearch.setLibelle(libelle);
        articlesSearch.setVolume(volume);
        articlesSearch.setTitrageMin(titrageMin);
        articlesSearch.setTitrageMax(titrageMax);
        articlesSearch.setMarque(marque);
        articlesSearch.setCouleur(couleur);
        articlesSearch.setFabricant(fabricant);
        articlesSearch.setType(type);
        articlesSearch.setPays(pays);
        articlesSearch.setContinent(continent);

    }

    public void updateArticle()
    {
        DAOFactory.getArticleDAO().update(articleSelected);
    }

    public void insertArticle()
    {
        DAOFactory.getArticleDAO().insert(articleSelected);
    }

    public void deleteArticle()
    {
        DAOFactory.getArticleDAO().delete(articleSelected);
    }

    public ObservableList<Article> filtrerArticles()
    {
        listArticlesFiltres = DAOFactory.getArticleDAO().filtrerArticles(articlesSearch);
        return listArticlesFiltres;
    }

    public ObservableList<Article> filtrerVente()
    {
        listArticlesVendus = DAOFactory.getVenteDAO().filtrerVente(articlesSearch);
        return listArticlesVendus;
    }

    public ObservableList<Double> getTitrage()
    {
        return DAOFactory.getArticleDAO().getTitrage(articlesSearch);
    }

    public ObservableList<Vente> getVentes()
    {
        return DAOFactory.getVenteDAO().getVentes(articlesSearch);
    }

    public ObservableList<Continent> getContinents()
    {
        return continents;
    }

    public ObservableList<Pays> getPays()
    {
        return pays;
    }

    public ObservableList<Fabricant> getFabricants()
    {
        return fabricants;
    }

    public ObservableList<Marque> getMarques()
    {
        return marques;
    }

    public ObservableList<Couleur> getCouleurs()
    {
        return couleurs;
    }

    public ObservableList<TypeBiere> getTypes()
    {
        return types;
    }

    public ObservableList<Article> getArticles()
    {
        return articles;
    }

    public ObservableList<Continent> getContinentsFiltrer()
    {
        return continentsFiltrer;
    }

    public ObservableList<Pays> getPaysFiltrer()
    {
        return paysFiltrer;
    }

    public ObservableList<Fabricant> getFabricantsFiltrer()
    {
        return fabricantsFiltrer;
    }

    public ObservableList<Marque> getMarquesFiltrer()
    {
        return marquesFiltrer;
    }

    public ObservableList<Couleur> getCouleursFiltrer()
    {
        return couleursFiltrer;
    }

    public ObservableList<TypeBiere> getTypesFiltrer()
    {
        return typesFiltrer;
    }
}