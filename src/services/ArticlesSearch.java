package services;

import job.*;

public class ArticlesSearch
    {

    private int id;
    private String libelle;
    private String volume;
    private Double titrageMin;
    private Double titrageMax;
    private float prix;
    private Couleur couleur;
    private Marque marque;
    private TypeBiere type;
    private Pays pays;
    private Continent continent;
    private Fabricant fabricant;


    public ArticlesSearch()
    {
        // empty
    }


    public Fabricant getFabricant()
    {
        return fabricant;
    }

    public void setFabricant(Fabricant fabricant)
    {
        this.fabricant = fabricant;
    }

    public Pays getPays()
    {
        return pays;
    }

    public void setPays(Pays pays)
    {
        this.pays = pays;
    }

    public Continent getContinent()
    {
        return continent;
    }

    public void setContinent(Continent continent)
    {
        this.continent = continent;
    }

    public String getLibelle()
    {
        return libelle;
    }

    public void setLibelle(String libelle)
    {
        this.libelle = libelle;
    }

    public String getVolume()
    {
        return volume;
    }

    public void setVolume(String volume)
    {
        this.volume = volume;
    }

    public Double getTitrageMin()
    {
        return titrageMin;
    }

    public void setTitrageMin(Double titrageMin)
    {
        this.titrageMin = titrageMin;
    }

    public Double getTitrageMax()
    {
        return titrageMax;
    }

    public void setTitrageMax(Double titrageMax)
    {
        this.titrageMax = titrageMax;
    }

    public float getPrix()
    {
        return prix;
    }

    public void setPrix(float prix)
    {
        this.prix = prix;
    }

    public Couleur getCouleur()
    {
        return couleur;
    }

    public void setCouleur(Couleur couleur)
    {
        this.couleur = couleur;
    }

    public Marque getMarque()
    {
        return marque;
    }

    public void setMarque(Marque marque)
    {
        this.marque = marque;
    }

    public TypeBiere getType()
    {
        return type;
    }

    public void setType(TypeBiere type)
    {
        this.type = type;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }


    }
