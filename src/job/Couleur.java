package job;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Couleur
{
	private int id;
	private String libelle;

	public Couleur(int id, String libelle)
	{
		super(); 
		this.id = id;
		this.libelle = libelle;
	} 
	
	public Couleur()
	{
		super();
	}  

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getLibelle()
	{
		return libelle;
	}
	
	public StringProperty getLibelleProperty()
	{
		return new SimpleStringProperty(libelle);
	}

	public void setLibelle(String libelle)
	{
		this.libelle = libelle;
	}

	@Override
	public String toString()
	{
		return libelle;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Couleur other = (Couleur) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
	
}
