package job;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Vente {

	private String annee;
	private int quantite;

	public Vente(String annee, int quantite) {
		super();
		this.annee = annee;
		this.quantite = quantite;
	}

	public String getAnnee() {
		return annee;
	}

	public void setAnnee(String annee) {
		this.annee = annee;
	}

	public StringProperty getAnneeProperty() {
		return new SimpleStringProperty(annee);
	}

	public int getQuantite() {
		return quantite;
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}

	@Override
	public String toString() {
		return annee;
	}

}
