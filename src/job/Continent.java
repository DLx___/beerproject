package job;

import java.util.ArrayList;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Continent
{
	private int id;
	private String libelle;
	private ObservableList<Pays> listePays;

	public Continent(int id, String libelle)
	{
		super();
		this.id = id;
		this.libelle = libelle;
		listePays = FXCollections.observableArrayList();
	}

	
	public ObservableList<Pays> getListePays()
	{
		return listePays;
	}

	public void setListePays(ObservableList<Pays> listePays)
	{
		this.listePays = listePays;
	}


	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getLibelle()
	{
		return libelle;
	}
	
	public StringProperty getLibelleProperty()
	{
		return new SimpleStringProperty(libelle);
	}

	public void setLibelle(String libelle)
	{
		this.libelle = libelle;
	}

	@Override
	public String toString()
	{
		return libelle;
	}
}
