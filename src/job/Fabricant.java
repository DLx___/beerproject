package job;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Fabricant {
	private int id;
	private String libelle;
	private ObservableList<Marque> listeMarque;

	public Fabricant(int id, String libelle) {
		super();
		this.id = id;
		this.libelle = libelle;
		this.listeMarque = FXCollections.observableArrayList();
	}

	public Fabricant() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public StringProperty getLibelleProperty() {
		return new SimpleStringProperty(libelle);
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public ObservableList<Marque> getListeMarque() {
		return listeMarque;
	}

	public void setListeMarque(ObservableList<Marque> listeMarque) {
		this.listeMarque = listeMarque;
	}

	@Override
	public String toString() {
		return libelle;
	}

	public void setMarque(Marque marque) {
		listeMarque.add(marque);
	}

	public void removeMarque(Marque marque) {
		listeMarque.remove(marque);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fabricant other = (Fabricant) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
