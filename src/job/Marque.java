package job;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Marque
{
	private int id;
	private String libelle;
	private ObjectProperty<Fabricant> fabricant;
	private ObjectProperty<Pays> pays;

	public Marque(int id, String libelle, Pays pays, Fabricant fabricant)
	{
		super();
		this.id = id;
		this.libelle = libelle;
		this.fabricant = new SimpleObjectProperty<>(fabricant);
		this.pays = new SimpleObjectProperty<>(pays);
	}

	public Marque(int id, String libelle)
	{
		super();
		this.id = id;
		this.libelle = libelle;
	}

	public Marque()
	{
		super();
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getLibelle()
	{
		return libelle;
	}

	public StringProperty getLibelleProperty()
	{
		return new SimpleStringProperty(libelle);
	}

	public void setLibelle(String libelle)
	{
		this.libelle = libelle;
	}

	public final ObjectProperty<Fabricant> fabricantProperty()
	{
		return this.fabricant;
	}

	public final Fabricant getFabricant()
	{
		return this.fabricantProperty().get();
	}

	public final void setFabricant(final Fabricant fabricant)
	{
		this.fabricantProperty().set(fabricant);
	}

	public final ObjectProperty<Pays> paysProperty()
	{
		return this.pays;
	}

	public final Pays getPays()
	{
		return this.paysProperty().get();
	}

	public final void setPays(final Pays pays)
	{
		this.paysProperty().set(pays);
	}

	@Override
	public String toString()
	{
		return libelle;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Marque other = (Marque) obj;
		if (id != other.id)
			return false;
		return true;
	}

	
}
