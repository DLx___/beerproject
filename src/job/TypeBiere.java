package job;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class TypeBiere
{
	private int id;
	private String libelle;

	public TypeBiere(int id, String libelle)
	{
		super();
		this.id = id;
		this.libelle = libelle;
	}

	public TypeBiere()
	{
		super();
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getLibelle()
	{
		return libelle;
	}

	public StringProperty getLibelleProperty()
	{
		return new SimpleStringProperty(libelle);
	}

	public void setLibelle(String libelle)
	{
		this.libelle = libelle;
	}

	@Override
	public String toString()
	{
		return libelle;
	}
}
