package job;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Pays
{
	private int id;
	private String libelle;
	private ObjectProperty<Continent> continent;

	public Pays(int id, String libelle, Continent continent)
	{
		super();
		this.id = id;
		this.libelle = libelle;
		this.continent = new SimpleObjectProperty<>(continent);
	}
	
	public Pays(int id, String libelle)
	{
		super();
		this.id = id;
		this.libelle = libelle;
	}

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public String getLibelle()
	{
		return libelle;
	}

	public StringProperty getLibelleProperty()
	{
		return new SimpleStringProperty(libelle);
	}

	public void setLibelle(String libelle)
	{
		this.libelle = libelle;
	}

	public final ObjectProperty<Continent> continentProperty()
	{
		return this.continent;
	}

	public final Continent getContinent()
	{
		return this.continentProperty().get();
	}

	public final void setContinent(final Continent continent)
	{
		this.continentProperty().set(continent);
	}

	@Override
	public String toString()
	{
		return libelle;
	}
}
