package job;

import java.math.BigDecimal;

import javafx.beans.property.FloatProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Article {
	private int id;
	private StringProperty libelle;
	private IntegerProperty volume;
	private FloatProperty titrage;
	private ObjectProperty<BigDecimal> prix;
	private ObjectProperty<Couleur> couleur;
	private ObjectProperty<Marque> marque;
	private ObjectProperty<TypeBiere> type;
	private Vente vente;

	public Article(int id, String libelle, int volume, float titrage, BigDecimal prix) {
		super();

		this.libelle = new SimpleStringProperty();
		this.volume = new SimpleIntegerProperty();
		this.titrage = new SimpleFloatProperty();
		this.prix = new SimpleObjectProperty<>();
		this.couleur = new SimpleObjectProperty<>();
		this.marque = new SimpleObjectProperty<>();
		this.type = new SimpleObjectProperty<>();

		this.id = id;
		this.libelle.set(libelle);
		this.volume.set(volume);
		this.titrage.set(titrage);
		this.prix.set(prix);

	}
	
//	public Article(int id, String libelle, int volume, float titrage, BigDecimal prix, Vente vente) {
//		super();
//
//
//		this.libelle = new SimpleStringProperty();
//		this.volume = new SimpleIntegerProperty();
//		this.titrage = new SimpleFloatProperty();
//		this.prix = new SimpleObjectProperty<>();
//		this.couleur = new SimpleObjectProperty<>();
//		this.marque = new SimpleObjectProperty<>();
//		this.type = new SimpleObjectProperty<>();
//
//		this.id = id;
//		this.libelle.set(libelle);
//		this.volume.set(volume);
//		this.titrage.set(titrage);
//		this.prix.set(prix);
//
//	}

	public Article() {

	}

	/*--------------------------------------------------------------------- Getter/Setter --------------------------------------------------------------------------------*/

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	/*--------------------------------------------------------------- Getter/Setter Property --------------------------------------------------------------------------------*/

	public final StringProperty libelleProperty() {
		return this.libelle;
	}

	public final String getLibelle() {
		return this.libelleProperty().get();
	}

	public final void setLibelle(final String libelle) {
		this.libelleProperty().set(libelle);
	}

	public final IntegerProperty volumeProperty() {
		return this.volume;
	}

	public final int getVolume() {
		return this.volumeProperty().get();
	}

	public final void setVolume(final int volume) {
		this.volumeProperty().set(volume);
	}

	public final FloatProperty titrageProperty() {
		return this.titrage;
	}

	public final float getTitrage() {
		return this.titrageProperty().get();
	}

	public final void setTitrage(final float titrage) {
		this.titrageProperty().set(titrage);
	}

	public final ObjectProperty<BigDecimal> prixProperty() {
		return this.prix;
	}

	public final BigDecimal getPrix() {
		return this.prixProperty().get();
	}

	public final void setPrix(final BigDecimal prix) {
		this.prixProperty().set(prix);
	}

	public final ObjectProperty<Couleur> couleurProperty() {
		return this.couleur;
	}

	public final Couleur getCouleur() {
		return this.couleurProperty().get();
	}

	public final void setCouleur(final Couleur couleur) {
		this.couleurProperty().set(couleur);
	}

	public final ObjectProperty<Marque> marqueProperty() {
		return this.marque;
	}

	public final Marque getMarque() {
		return this.marqueProperty().get();
	}

	public final void setMarque(final Marque marque) {
		this.marqueProperty().set(marque);
	}

	public final ObjectProperty<TypeBiere> typeProperty() {
		return this.type;
	}

	public final TypeBiere getType() {
		return this.typeProperty().get();
	}

	public final void setType(final TypeBiere type) {
		this.typeProperty().set(type);
	}

	public Vente getVente() {
		return vente;
	}

	public void setVente(Vente vente) {
		this.vente = vente;
	}

	@Override
	public String toString() {
		return libelle.toString() + "" + volume.toString() + "" + titrage.toString() + "" + prix.toString() + ""
				+ couleur.toString() + "" + marque.toString() + "" + type.toString();
	}

}
