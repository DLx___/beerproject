package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import job.TypeBiere;

public class TypeBiereDAO extends DAO<TypeBiere>
{
	public TypeBiereDAO(Connection connexion)
	{
		super(connexion);
	}

	@Override
	public TypeBiere getById(int id)
	{
		ResultSet rs;
		TypeBiere type = null;

		try
		{
			Statement stmt = connexion.createStatement();
			String cmd = "SELECT * FROM TYPEBIERE AS T JOIN ARTICLES AS A on A.ID_TYPEBIERE= T.ID_TYPE WHERE T.ID_TYPE ="
					+ id;
			rs = stmt.executeQuery(cmd);
			type = dataGetTypes(rs);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return type;
	}

	@Override
	public ArrayList<TypeBiere> getAll()
	{
		ResultSet rs;
		ArrayList<TypeBiere> liste = new ArrayList<>();

		try
		{
			Statement stmt = connexion.createStatement();
			String strCmd = "SELECT * FROM TYPEBIERE ORDER BY NOM_TYPE";
			rs = stmt.executeQuery(strCmd);

			while (rs.next())
			{
				liste.add(dataGetTypes(rs));
			}
			rs.close();

		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return liste;
	}

	private TypeBiere dataGetTypes(ResultSet rs) throws SQLException
	{
		return new TypeBiere(rs.getInt(1), rs.getString(2));
	}

	@Override
	public boolean insert(TypeBiere objet)
	{
		boolean isOk = false;

		try
		{
			Statement stmt = connexion.createStatement();
			String cmd = "INSERT INTO TYPEBIERE (ID_TYPE,NOM_TYPE) VALUES (" + objet.getId() + "," + objet.getLibelle()
					+ ")";
			isOk = stmt.executeQuery(cmd) != null;
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return isOk;
	}

	@Override
	public boolean update(TypeBiere objet)
	{
		boolean isOk = false;

		try
		{
			Statement stmt = connexion.createStatement();
			String cmd = "UPDATE FROM TYPEBIERE SET VALUES (" + objet.getId() + "," + objet.getLibelle()
					+ ") WHERE ID_TYPE = " + objet.getId();
			isOk = stmt.executeQuery(cmd) != null;
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return isOk;
	}

	@Override
	public boolean delete(TypeBiere objet)
	{
		boolean isOk = false;

		try
		{
			Statement stmt = connexion.createStatement();
			String cmd = "DELETE FROM TYPEBIERE WHERE ID_TYPE = (" + objet.getLibelle() + ")";
			isOk = stmt.executeQuery(cmd) != null;
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return isOk;
	}

}