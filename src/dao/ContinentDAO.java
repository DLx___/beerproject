package dao;

import job.Continent;
import job.Pays;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class ContinentDAO extends DAO<Continent>
{
    public ContinentDAO(Connection connexion)
    {
        super(connexion);
    }

    @Override
    public Continent getById(int id)
    {
        ResultSet rs;
        Continent continent = null;

        try
        {
            Statement stmt = connexion.createStatement();
            String cmd = "SELECT * FROM CONTINENT WHERE ID_CONTINENT = " + id;
            rs = stmt.executeQuery(cmd);
            continent = dataGetContinents(rs);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return continent;
    }

    @Override
    public ArrayList<Continent> getAll()
    {
        ResultSet rs;
        ArrayList<Continent> liste = new ArrayList<>();
        Continent continentLu = new Continent(0, "");
        try
        {
            try (Statement stmt = connexion.createStatement())
            {
                String strCmd = "SELECT * FROM CONTINENT AS C JOIN PAYS AS P on C.ID_CONTINENT = P.ID_CONTINENT ORDER BY C.ID_CONTINENT";
                rs = stmt.executeQuery(strCmd);

                while (rs.next())
                {
                    if (continentLu.getId() != rs.getInt(1))
                    {
                        continentLu = dataGetContinents(rs);
                        liste.add(continentLu);
                    }
                    continentLu.getListePays().add(new Pays(rs.getInt(3), rs.getString(4), dataGetContinents(rs)));
                }
            }


            rs.close();

        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return liste;
    }

    private Continent dataGetContinents(ResultSet rs) throws SQLException
    {
        return new Continent(rs.getInt(1), rs.getString(2));
    }

    @Override
    public boolean insert(Continent objet)
    {
        boolean isOk = false;

        try
        {
            Statement stmt = connexion.createStatement();
            String cmd = "INSERT INTO CONTINENT (ID_CONTINENT,NOM_CONTINENT) VALUES (" + objet.getId() + "," + objet.getLibelle() + ")";
            isOk = stmt.executeQuery(cmd) != null;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return isOk;
    }

    @Override
    public boolean update(Continent objet)
    {
        boolean isOk = false;

        try
        {
            Statement stmt = connexion.createStatement();
            String cmd = "UPDATE FROM CONTINENT SET (" + objet.getId() + "," + objet.getLibelle() + ") WHERE ID_CONTINENT = " + objet.getId();
            isOk = stmt.executeQuery(cmd) != null;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return isOk;
    }

    @Override
    public boolean delete(Continent objet)
    {
        boolean isOk = false;

        try
        {
            Statement stmt = connexion.createStatement();
            String cmd = "DELETE FROM CONTINENT WHERE ID_CONTINENT = (" + objet.getLibelle() + ")";
            isOk = stmt.executeQuery(cmd) != null;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return isOk;
    }

}