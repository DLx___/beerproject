package dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import job.*;
import services.ArticlesSearch;

import java.sql.*;
import java.util.ArrayList;

public class VenteDAO extends DAO<Vente>
    {

    public VenteDAO(Connection connexion)
    {
        super(connexion);
    }

    @Override
    public Vente getById(int id)
    {
        ResultSet rs;
        Vente vente = null;
        String cmd = "SELECT ANNEE, SUM(QUANTITE) as QUANTITE FROM VENDRE AS V WHERE V.ID_ARTICLES = " + id + " GROUP BY V.ANNEE";

        try
        {
            Statement stmt = connexion.createStatement();
            rs = stmt.executeQuery(cmd);
            vente = dataGetVentes(rs);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return vente;

    }

    @Override
    public ArrayList<Vente> getAll()
    {
        ResultSet rs;
        ArrayList<Vente> liste = new ArrayList<>();
        String strCmd = "SELECT ANNEE, SUM(QUANTITE) as QUANTITE FROM VENDRE AS V GROUP BY V.ANNEE";

        try (Statement stmt = connexion.createStatement())
        {
            rs = stmt.executeQuery(strCmd);
            while (rs.next())
            {
                liste.add(dataGetVentes(rs));

            }
            rs.close();
        }


        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return liste;
    }

    public ObservableList<Vente> getVentes(ArticlesSearch articlesSearch)
    {

        ArrayList<Vente> liste = new ArrayList<>();
        ResultSet rs;
        String strCmd = "SELECT ANNEE, SUM(QUANTITE) as QUANTITE FROM VENDRE AS V GROUP BY V.ANNEE";

        try (Statement stmt = connexion.createStatement())
        {

            rs = stmt.executeQuery(strCmd);
            while (rs.next())
            {

                liste.add(new Vente(rs.getString(1), rs.getInt(2)));
            }
            stmt.execute(strCmd);

            rs.close();
        }


        catch (SQLException e)
        {

            e.printStackTrace();
        }

        return FXCollections.observableArrayList(liste);
    }

    public ObservableList<Article> filtrerVente(ArticlesSearch articlesSearch)
    {
        ArrayList<Article> listeArticles = new ArrayList<>();
        String procedureStockee = "{call dbo.sp_itemsSells(?,?,?,?,?,?,?,?,?,?)}";
        ResultSet rs;

        try (CallableStatement sctmt = this.connexion.prepareCall(procedureStockee))
        {
            System.out.println("Libellé : " + articlesSearch.getLibelle());
            System.out.println("Volume : " + articlesSearch.getVolume());
            System.out.println("Marque : " + articlesSearch.getMarque());
            System.out.println("TitrageMin : " + articlesSearch.getTitrageMin());
            System.out.println("TitrageMax : " + articlesSearch.getTitrageMax());
            System.out.println("Couleur : " + articlesSearch.getCouleur());
            System.out.println("Fabricant : " + articlesSearch.getFabricant());
            System.out.println("Type : " + articlesSearch.getType());
            System.out.println("Pays : " + articlesSearch.getPays());
            System.out.println("Continent : " + articlesSearch.getContinent());
            System.out.printf("%n");
            System.out.printf("%n");

            getLibelleSearch(articlesSearch, sctmt);
            getVolumeSearch(articlesSearch, sctmt);
            getMarqueSearch(articlesSearch, sctmt);
            getFabricantSearch(articlesSearch, sctmt);
            getTitrageMinSearch(articlesSearch, sctmt);
            getTitrageMaxSearch(articlesSearch, sctmt);
            getCouleurSearch(articlesSearch, sctmt);
            getTypeSearch(articlesSearch, sctmt);
            getPaysSearch(articlesSearch, sctmt);
            getContinentSearch(articlesSearch, sctmt);

            sctmt.execute();
            rs = sctmt.getResultSet();

            while (rs.next())
            {

                Article article = new Article(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getFloat(4), rs.getBigDecimal(5));
                article.setCouleur(new Couleur(rs.getInt(6), rs.getString(7)));
                article.setMarque(new Marque(rs.getInt(10), rs.getString(11), new Pays(rs.getInt(14), rs.getString(15), new Continent(rs.getInt(16), rs.getString(17))), new Fabricant(rs.getInt(12), rs.getString(13))

                ));
                article.setType(new TypeBiere(rs.getInt(8), rs.getString(9)));

                listeArticles.add(article);

            }

            rs.close();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return FXCollections.observableArrayList(listeArticles);
    }

    private Vente dataGetVentes(ResultSet rs) throws SQLException
    {
        return new Vente(rs.getString(1), rs.getInt(2));
    }

    @Override
    public boolean insert(Vente objet)
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean update(Vente objet)
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean delete(Vente objet)
    {
        // TODO Auto-generated method stub
        return false;
    }

    private void getLibelleSearch(ArticlesSearch articlesSearch, CallableStatement sctmt) throws SQLException
    {
        if (articlesSearch.getLibelle() == null)
        {
            sctmt.setNull(1, java.sql.Types.NVARCHAR);
        }
        else
        {
            sctmt.setString(1, articlesSearch.getLibelle());
        }
    }

    private void getVolumeSearch(ArticlesSearch articlesSearch, CallableStatement sctmt) throws SQLException
    {
        if (articlesSearch.getVolume() == null || articlesSearch.getVolume().equals("Afficher tous"))
        {
            sctmt.setNull(2, java.sql.Types.INTEGER);
        }
        else
        {
            sctmt.setInt(2, Integer.parseInt(articlesSearch.getVolume()));
        }
    }

    private void getTitrageMinSearch(ArticlesSearch articlesSearch, CallableStatement sctmt) throws SQLException
    {
        if (articlesSearch.getTitrageMin() == 0)
        {
            sctmt.setNull(3, java.sql.Types.FLOAT);
        }
        else
        {
            sctmt.setDouble(3, articlesSearch.getTitrageMin());
        }
    }

    private void getTitrageMaxSearch(ArticlesSearch articlesSearch, CallableStatement sctmt) throws SQLException
    {
        if (articlesSearch.getTitrageMax() == 0)
        {
            sctmt.setNull(4, java.sql.Types.FLOAT);
        }
        else
        {
            sctmt.setDouble(4, articlesSearch.getTitrageMax());

        }
    }

    private void getMarqueSearch(ArticlesSearch articlesSearch, CallableStatement sctmt) throws SQLException
    {
        if (articlesSearch.getMarque() == null || articlesSearch.getMarque().getId() == 0)
        {
            sctmt.setNull(5, java.sql.Types.INTEGER);
        }
        else
        {
            sctmt.setInt(5, articlesSearch.getMarque().getId());
        }
    }

    private void getCouleurSearch(ArticlesSearch articlesSearch, CallableStatement sctmt) throws SQLException
    {
        if (articlesSearch.getCouleur() == null || articlesSearch.getCouleur().getId() == 0)
        {
            sctmt.setNull(6, java.sql.Types.INTEGER);
        }
        else
        {
            sctmt.setInt(6, articlesSearch.getCouleur().getId());

        }
    }

    private void getFabricantSearch(ArticlesSearch articlesSearch, CallableStatement sctmt) throws SQLException
    {
        if (articlesSearch.getFabricant() == null || articlesSearch.getFabricant().getId() == 0)
        {
            sctmt.setNull(7, java.sql.Types.INTEGER);
        }
        else
        {
            sctmt.setInt(7, articlesSearch.getFabricant().getId());

        }
    }

    private void getTypeSearch(ArticlesSearch articlesSearch, CallableStatement sctmt) throws SQLException
    {
        if (articlesSearch.getType() == null || articlesSearch.getType().getId() == 0)
        {
            sctmt.setNull(8, java.sql.Types.INTEGER);
        }
        else
        {
            sctmt.setDouble(8, articlesSearch.getType().getId());

        }
    }

    private void getPaysSearch(ArticlesSearch articlesSearch, CallableStatement sctmt) throws SQLException
    {
        if (articlesSearch.getPays() == null || articlesSearch.getPays().getId() == 0)
        {
            sctmt.setNull(9, java.sql.Types.INTEGER);
        }
        else
        {
            sctmt.setInt(9, articlesSearch.getPays().getId());

        }
    }

    private void getContinentSearch(ArticlesSearch articlesSearch, CallableStatement sctmt) throws SQLException
    {
        if (articlesSearch.getContinent() == null || articlesSearch.getContinent().getId() == 0)
        {
            sctmt.setNull(10, java.sql.Types.INTEGER);
        }
        else
        {
            sctmt.setInt(10, articlesSearch.getContinent().getId());

        }
    }

    }
