package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import job.Couleur;

public class CouleurDAO extends DAO<Couleur>
{
	public CouleurDAO(Connection connexion)
	{
		super(connexion);
	}

	@Override
	public Couleur getById(int id)
	{
		ResultSet rs;
		Couleur color = null;

		try
		{
			Statement stmt = connexion.createStatement();
			String cmd = "SELECT * FROM COULEUR AS C JOIN ARTICLES AS A on A.ID_COULEUR= C.ID_COULEUR WHERE A.ID_COULEUR = "
					+ id;
			rs = stmt.executeQuery(cmd);
			color = dataGetCouleurs(rs);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return color;
	}

	@Override
	public ArrayList<Couleur> getAll()
	{
		ResultSet rs;
		ArrayList<Couleur> liste = new ArrayList<>();

		try
		{
			Statement stmt = connexion.createStatement();
			String strCmd = "SELECT * FROM COULEUR ORDER BY NOM_COULEUR";
			rs = stmt.executeQuery(strCmd);

			while (rs.next())
			{
				liste.add(dataGetCouleurs(rs));
			}
			rs.close();

		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return liste;
	}

	private Couleur dataGetCouleurs(ResultSet rs) throws SQLException
	{
		return new Couleur(rs.getInt(1), rs.getString(2));
	}

	@Override
	public boolean insert(Couleur objet)
	{
		boolean isOk = false;

		try
		{
			Statement stmt = connexion.createStatement();
			String cmd = "INSERT INTO COULEUR (ID_COULEUR,NOM_COULEUR) VALUES (" + objet.getId() + ","
					+ objet.getLibelle() + ")";
			isOk = stmt.executeQuery(cmd) != null;
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return isOk;
	}

	@Override
	public boolean update(Couleur objet)
	{
		boolean isOk = false;

		try
		{
			Statement stmt = connexion.createStatement();
			String cmd = "UPDATE FROM COULEUR SET VALUES (" + objet.getId() + "," + objet.getLibelle()
					+ ") WHERE ID_COULEUR = " + objet.getId();
			isOk = stmt.executeQuery(cmd) != null;
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return isOk;
	}

	@Override
	public boolean delete(Couleur objet)
	{
		boolean isOk = false;

		try
		{
			Statement stmt = connexion.createStatement();
			String cmd = "DELETE FROM COULEUR WHERE ID_COULEUR = (" + objet.getLibelle() + ")";
			isOk = stmt.executeQuery(cmd) != null;
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return isOk;
	}

}