package dao;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import job.*;
import services.ArticlesSearch;

import java.sql.*;
import java.util.ArrayList;

public class ArticleDAO extends DAO<Article>
{

    private Article article;

    public ArticleDAO(Connection connexion)
    {
        super(connexion);
    }

    @Override
    public Article getById(int id)
    {
        ResultSet rs;
        try
        {
            Statement stmt = connexion.createStatement();
            String cmd = "SELECT * FROM VIEW_ITEMS WHERE ID_ARTICLES = " + id;
            rs = stmt.executeQuery(cmd);
            dataGetArticles(rs);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return article;
    }

    @Override
    public ArrayList<Article> getAll()
    {
        ResultSet rs;
        ArrayList<Article> liste = new ArrayList<>();

        try
        {
            Statement stmt = connexion.createStatement();
            String strCmd = "SELECT * FROM VIEW_ITEMS ORDER BY ID_ARTICLES";
            rs = stmt.executeQuery(strCmd);

            while (rs.next())
            {
                dataGetArticles(rs);
                liste.add(article);
            }
            rs.close();

        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return liste;
    }

    private void dataGetArticles(ResultSet rs) throws SQLException
    {
        article = new Article(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getFloat(4), rs.getBigDecimal(5));
        article.setCouleur(new Couleur(rs.getInt(6), rs.getString(7)));
        article.setMarque(new Marque(rs.getInt(10), rs.getString(11), new Pays(rs.getInt(14), rs.getString(15), new Continent(rs.getInt(16), rs.getString(17))), new Fabricant(rs.getInt(12), rs.getString(13))));
        article.setType(new TypeBiere(rs.getInt(8), rs.getString(9)));

    }

    @Override
    public boolean insert(Article article)
    {
        boolean isOk = false;

        try
        {
            String cmd = "INSERT INTO ARTICLES (NOM_ARTICLES, PRIX_ACHAT, VOLUME, TITRAGE, ID_COULEUR, ID_TYPEBIERE, ID_MARQUE) VALUES (?,?,?,?,?,?,?)";
            PreparedStatement pStatement = connexion.prepareStatement(cmd);
            // isOk = pStatement.executeQuery(cmd) != null;

            pStatement.setString(1, article.getLibelle());

            if (article.getPrix() == null)
            {
                pStatement.setNull(2, java.sql.Types.FLOAT);
            }
            else
            {
                pStatement.setBigDecimal(2, article.getPrix());
            }
            if (article.getVolume() == 0)
            {
                pStatement.setNull(3, java.sql.Types.INTEGER);
            }
            else
            {
                pStatement.setInt(3, article.getVolume());
            }

            if (article.getTitrage() == 0)
            {
                pStatement.setNull(4, java.sql.Types.FLOAT);
            }
            else
            {
                pStatement.setFloat(4, article.getTitrage());
            }

            if (article.getCouleur().getId() == 0)
            {
                pStatement.setNull(5, java.sql.Types.INTEGER);
            }
            else
            {
                pStatement.setInt(5, article.getId());
            }

            pStatement.setInt(6, article.getType().getId());
            pStatement.setInt(7, article.getMarque().getId());
            pStatement.executeUpdate();
            //isOk = pStatement.executeQuery(cmd) != null;
            pStatement.close();

        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return isOk;
    }

    @Override
    public boolean update(Article article)
    {
        boolean isOk = false;

        try
        {
            String cmd = "UPDATE ARTICLES SET NOM_ARTICLES = ? ,PRIX_ACHAT = ? ,VOLUME = ? ,TITRAGE = ? ,ID_COULEUR = ? ,ID_TYPEBIERE = ? ,ID_MARQUE = ? WHERE ID_ARTICLES = ?";
            PreparedStatement pStatement = connexion.prepareStatement(cmd);

            pStatement.setString(1, article.getLibelle());

            if (article.getPrix() == null)
            {
                pStatement.setNull(2, java.sql.Types.FLOAT);
            }
            else
            {
                pStatement.setBigDecimal(2, article.getPrix());
            }

            if (article.getVolume() == 0)
            {
                pStatement.setNull(3, java.sql.Types.INTEGER);
            }
            else
            {
                pStatement.setInt(3, article.getVolume());
            }

            if (article.getTitrage() == 0)
            {
                pStatement.setNull(4, java.sql.Types.FLOAT);
            }
            else
            {
                pStatement.setFloat(4, article.getTitrage());
            }

            if (article.getCouleur().getId() == 0)
            {
                pStatement.setNull(5, java.sql.Types.INTEGER);
            }
            else
            {
                pStatement.setInt(5, article.getId());
            }

            pStatement.setInt(6, article.getType().getId());
            pStatement.setInt(7, article.getMarque().getId());
            pStatement.setInt(8, article.getId());

            pStatement.executeUpdate();
            // isOk = pStatement.executeUpdate(cmd) != null;
            pStatement.close();

        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return isOk;
    }

    @Override
    public boolean delete(Article article)
    {
        boolean isOk = false;

        try
        {
            String cmd = "DELETE FROM ARTICLES WHERE ID_ARTICLES = ?";
            PreparedStatement pStatement = connexion.prepareStatement(cmd);
            pStatement.setInt(1, article.getId());
            pStatement.execute();
            // isOk = pStatement.executeQuery(cmd) != null;
            pStatement.close();

        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return isOk;
    }

    public ObservableList<Article> filtrerArticles(ArticlesSearch articlesSearch)
    {
        ArrayList<Article> listeArticles = new ArrayList<>();
        ResultSet rs;
        try
        {
            String procedureStockee = "{call dbo.sp_itemsQBE(?,?,?,?,?,?,?,?,?,?)}";
            CallableStatement sctmt = this.connexion.prepareCall(procedureStockee);
            System.out.println("Libell� : " + articlesSearch.getLibelle());
            System.out.println("Volume : " + articlesSearch.getVolume());
            System.out.println("Marque : " + articlesSearch.getMarque());
            System.out.println("TitrageMin : " + articlesSearch.getTitrageMin());
            System.out.println("TitrageMax : " + articlesSearch.getTitrageMax());
            System.out.println("Couleur : " + articlesSearch.getCouleur());
            System.out.println("Fabricant : " + articlesSearch.getFabricant());
            System.out.println("Type : " + articlesSearch.getType());
            System.out.println("Pays : " + articlesSearch.getPays());
            System.out.println("Continent : " + articlesSearch.getContinent());
            System.out.printf("%n");
            System.out.printf("%n");

            getLibelleSearch(articlesSearch, sctmt);
            getVolumeSearch(articlesSearch, sctmt);
            getMarqueSearch(articlesSearch, sctmt);
            getFabricantSearch(articlesSearch, sctmt);
            getTitrageMinSearch(articlesSearch, sctmt);
            getTitrageMaxSearch(articlesSearch, sctmt);
            getCouleurSearch(articlesSearch, sctmt);
            getTypeSearch(articlesSearch, sctmt);
            getPaysSearch(articlesSearch, sctmt);
            getContinentSearch(articlesSearch, sctmt);

            sctmt.execute();
            rs = sctmt.getResultSet();

            while (rs.next())
            {

                article = new Article(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getFloat(4), rs.getBigDecimal(5));
                article.setCouleur(new Couleur(rs.getInt(6), rs.getString(7)));
                article.setMarque(new Marque(rs.getInt(10), rs.getString(11), new Pays(rs.getInt(14), rs.getString(15), new Continent(rs.getInt(16), rs.getString(17))), new Fabricant(rs.getInt(12), rs.getString(13))

                ));
                article.setType(new TypeBiere(rs.getInt(8), rs.getString(9)));

                listeArticles.add(article);

            }

        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return FXCollections.observableArrayList(listeArticles);
    }

    public ObservableList<Double> getTitrage(ArticlesSearch articlesSearch)
    {
        ArrayList<Double> listeArticles = new ArrayList<>();
        ResultSet rs;

        try
        {
            String procedureStockee = "{call dbo.sp_titration_min_max(?,?,?,?,?,?,?,?)}";
            CallableStatement sctmt = this.connexion.prepareCall(procedureStockee);

            getLibelleSearch(articlesSearch, sctmt);
            getVolumeSearch(articlesSearch, sctmt);

            if (articlesSearch.getMarque() == null || articlesSearch.getMarque().getId() == 0)
            {
                sctmt.setNull(3, java.sql.Types.INTEGER);
            }
            else
            {
                sctmt.setInt(3, articlesSearch.getMarque().getId());
            }

            if (articlesSearch.getCouleur() == null || articlesSearch.getCouleur().getId() == 0)
            {
                sctmt.setNull(4, java.sql.Types.INTEGER);
            }
            else
            {
                sctmt.setInt(4, articlesSearch.getCouleur().getId());

            }
            if (articlesSearch.getFabricant() == null || articlesSearch.getFabricant().getId() == 0)
            {
                sctmt.setNull(5, java.sql.Types.INTEGER);
            }
            else
            {
                sctmt.setInt(5, articlesSearch.getFabricant().getId());

            }

            if (articlesSearch.getType() == null || articlesSearch.getType().getId() == 0)
            {
                sctmt.setNull(6, java.sql.Types.INTEGER);
            }
            else
            {
                sctmt.setDouble(6, articlesSearch.getType().getId());

            }

            if (articlesSearch.getPays() == null || articlesSearch.getPays().getId() == 0)
            {
                sctmt.setNull(7, java.sql.Types.INTEGER);
            }
            else
            {
                sctmt.setInt(7, articlesSearch.getPays().getId());

            }

            if (articlesSearch.getContinent() == null || articlesSearch.getContinent().getId() == 0)
            {
                sctmt.setNull(8, java.sql.Types.INTEGER);
            }
            else
            {
                sctmt.setInt(8, articlesSearch.getContinent().getId());

            }

            sctmt.execute();
            rs = sctmt.getResultSet();

            while (rs.next())
            {

                listeArticles.add(0, rs.getDouble(1));
                listeArticles.add(1, rs.getDouble(2));
            }

        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return FXCollections.observableArrayList(listeArticles);
    }

    private void getLibelleSearch(ArticlesSearch articlesSearch, CallableStatement sctmt) throws SQLException
    {
        if (articlesSearch.getLibelle() == null)
        {
            sctmt.setNull(1, java.sql.Types.NVARCHAR);
        }
        else
        {
            sctmt.setString(1, articlesSearch.getLibelle());
        }
    }

    private void getVolumeSearch(ArticlesSearch articlesSearch, CallableStatement sctmt) throws SQLException
    {
        if (articlesSearch.getVolume() == null || articlesSearch.getVolume().equals("Afficher tous"))
        {
            sctmt.setNull(2, java.sql.Types.INTEGER);
        }
        else
        {
            sctmt.setInt(2, Integer.parseInt(articlesSearch.getVolume()));
        }
    }

    private void getTitrageMinSearch(ArticlesSearch articlesSearch, CallableStatement sctmt) throws SQLException
    {
        if (articlesSearch.getTitrageMin() == 0)
        {
            sctmt.setNull(3, java.sql.Types.FLOAT);
        }
        else
        {
            sctmt.setDouble(3, articlesSearch.getTitrageMin());
        }
    }

    private void getTitrageMaxSearch(ArticlesSearch articlesSearch, CallableStatement sctmt) throws SQLException
    {
        if (articlesSearch.getTitrageMax() == 0)
        {
            sctmt.setNull(4, java.sql.Types.FLOAT);
        }
        else
        {
            sctmt.setDouble(4, articlesSearch.getTitrageMax());

        }
    }

    private void getMarqueSearch(ArticlesSearch articlesSearch, CallableStatement sctmt) throws SQLException
    {
        if (articlesSearch.getMarque() == null || articlesSearch.getMarque().getId() == 0)
        {
            sctmt.setNull(5, java.sql.Types.INTEGER);
        }
        else
        {
            sctmt.setInt(5, articlesSearch.getMarque().getId());
        }
    }

    private void getCouleurSearch(ArticlesSearch articlesSearch, CallableStatement sctmt) throws SQLException
    {
        if (articlesSearch.getCouleur() == null || articlesSearch.getCouleur().getId() == 0)
        {
            sctmt.setNull(6, java.sql.Types.INTEGER);
        }
        else
        {
            sctmt.setInt(6, articlesSearch.getCouleur().getId());

        }
    }

    private void getFabricantSearch(ArticlesSearch articlesSearch, CallableStatement sctmt) throws SQLException
    {
        if (articlesSearch.getFabricant() == null || articlesSearch.getFabricant().getId() == 0)
        {
            sctmt.setNull(7, java.sql.Types.INTEGER);
        }
        else
        {
            sctmt.setInt(7, articlesSearch.getFabricant().getId());

        }
    }

    private void getTypeSearch(ArticlesSearch articlesSearch, CallableStatement sctmt) throws SQLException
    {
        if (articlesSearch.getType() == null || articlesSearch.getType().getId() == 0)
        {
            sctmt.setNull(8, java.sql.Types.INTEGER);
        }
        else
        {
            sctmt.setDouble(8, articlesSearch.getType().getId());

        }
    }

    private void getPaysSearch(ArticlesSearch articlesSearch, CallableStatement sctmt) throws SQLException
    {
        if (articlesSearch.getPays() == null || articlesSearch.getPays().getId() == 0)
        {
            sctmt.setNull(9, java.sql.Types.INTEGER);
        }
        else
        {
            sctmt.setInt(9, articlesSearch.getPays().getId());

        }
    }

    private void getContinentSearch(ArticlesSearch articlesSearch, CallableStatement sctmt) throws SQLException
    {
        if (articlesSearch.getContinent() == null || articlesSearch.getContinent().getId() == 0)
        {
            sctmt.setNull(10, java.sql.Types.INTEGER);
        }
        else
        {
            sctmt.setInt(10, articlesSearch.getContinent().getId());

        }
    }

}