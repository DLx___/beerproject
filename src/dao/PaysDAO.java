package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import job.Continent;
import job.Pays;

public class PaysDAO extends DAO<Pays>
{
	public PaysDAO(Connection connexion)
	{
		super(connexion);
	}

	@Override
	public Pays getById(int id)
	{
		ResultSet rs;
		Pays pays = null;

		try
		{
			Statement stmt = connexion.createStatement();
			String cmd = "SELECT * FROM PAYS AS P JOIN CONTINENT AS C ON C.ID_CONTINENT = P.ID_CONTINENT WHERE ID_PAYS = "
					+ id;
			rs = stmt.executeQuery(cmd);
			pays = dataGetPays(rs);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return pays;
	}

	@Override
	public ArrayList<Pays> getAll()
	{
		ResultSet rs;
		ArrayList<Pays> liste = new ArrayList<>();
		Pays pays;

		try
		{
			Statement stmt = connexion.createStatement();
			String strCmd = "SELECT * FROM PAYS AS P JOIN CONTINENT AS C ON C.ID_CONTINENT = P.ID_CONTINENT";
			rs = stmt.executeQuery(strCmd);

			
			while (rs.next())
			{
				liste.add(dataGetPays(rs));
			}
			rs.close();

		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return liste;
	}
	
	private Pays dataGetPays(ResultSet rs) throws SQLException
	{
		return new Pays(rs.getInt(1), rs.getString(2), new Continent(rs.getInt(4), rs.getString(5)));
	}

	@Override
	public boolean insert(Pays objet)
	{
		boolean isOk = false;

		try
		{
			Statement stmt = connexion.createStatement();
			String cmd = "INSERT INTO PAYS (ID_PAYS,NOM_PAYS) VALUES (" + objet.getId() + "," + objet.getLibelle()
					+ ")";
			isOk = stmt.executeQuery(cmd) != null;
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return isOk;
	}

	@Override
	public boolean update(Pays objet)
	{
		boolean isOk = false;

		try
		{
			Statement stmt = connexion.createStatement();
			String cmd = "UPDATE FROM PAYS SET VALUES (" + objet.getId() + "," + objet.getLibelle()
					+ ") WHERE ID_PAYS = " + objet.getId();
			isOk = stmt.executeQuery(cmd) != null;
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return isOk;
	}

	@Override
	public boolean delete(Pays objet)
	{
		boolean isOk = false;

		try
		{
			Statement stmt = connexion.createStatement();
			String cmd = "DELETE FROM PAYS WHERE ID_PAYS = (" + objet.getLibelle() + ")";
			isOk = stmt.executeQuery(cmd) != null;
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return isOk;
	}

}