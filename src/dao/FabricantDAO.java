package dao;

import job.Fabricant;
import job.Marque;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class FabricantDAO extends DAO<Fabricant>
    {

    public FabricantDAO(Connection connexion)
    {
        super(connexion);
    }

    @Override
    public Fabricant getById(int id)
    {
        ResultSet rs;
        Fabricant fabricant = null;

        try
        {
            Statement stmt = connexion.createStatement();
            String cmd = "SELECT * FROM FABRICANT WHERE ID_FABRICANT = " + id;
            rs = stmt.executeQuery(cmd);
            fabricant = new Fabricant(rs.getInt(1), rs.getString(2));
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return fabricant;
    }

    @Override
    public ArrayList<Fabricant> getAll()
    {
        ResultSet rs;
        ArrayList<Fabricant> liste = new ArrayList<>();
        Fabricant fabricantLu = new Fabricant(0, "");

        try
        {
            try (Statement stmt = connexion.createStatement())
            {
                String strCmd = "SELECT * FROM FABRICANT LEFT JOIN MARQUE ON MARQUE.ID_FABRICANT = FABRICANT.ID_FABRICANT ORDER BY NOM_FABRICANT";
                rs = stmt.executeQuery(strCmd);

                while (rs.next())
                {

                    if (fabricantLu.getId() != rs.getInt(1))
                    {
                        fabricantLu = new Fabricant(rs.getInt(1), rs.getString(2));
                        liste.add(fabricantLu);
                    }
                    fabricantLu.getListeMarque().add(new Marque(rs.getInt(3), rs.getString(4)));
                }
            }
            rs.close();

        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return liste;
    }

    @Override
    public boolean insert(Fabricant objet)
    {
        boolean isOk = false;

        try
        {
            Statement stmt = connexion.createStatement();
            String cmd = "INSERT INTO FABRICANT (ID_FABRICANT,NOM_FABRICANT) VALUES (" + objet.getId() + "," + objet.getLibelle() + ")";
            isOk = stmt.executeQuery(cmd) != null;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return isOk;
    }

    @Override
    public boolean update(Fabricant objet)
    {
        boolean isOk = false;

        try
        {
            Statement stmt = connexion.createStatement();
            String cmd = "UPDATE FROM FABRICANT SET VALUES (" + objet.getId() + "," + objet.getLibelle() + ") WHERE ID_FABRICANT = " + objet.getId();
            isOk = stmt.executeQuery(cmd) != null;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return isOk;
    }

    @Override
    public boolean delete(Fabricant objet)
    {
        boolean isOk = false;

        try
        {
            Statement stmt = connexion.createStatement();
            String cmd = "DELETE FROM FABRICANT WHERE ID_FABRICANT = (" + objet.getLibelle() + ")";
            isOk = stmt.executeQuery(cmd) != null;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return isOk;
    }

    }