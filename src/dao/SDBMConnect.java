package dao;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;

import java.sql.Connection;

public class SDBMConnect {
    // Declare the JDBC objects.
    private static Connection connexion;

    private SDBMConnect() {
        try {
            /**********************************************************************/
            /*----------------------------- PC-AFPA ------------------------------*/
            /**********************************************************************/

//			String dbURL = "jdbc:sqlserver://STA7400544:1433;databaseName=SDBM";
//			String user = "javaSDBM";
//			String pass = "javaSDBM";
//			connexion = DriverManager.getConnection(dbURL, user, pass);

			SQLServerDataSource ds = new SQLServerDataSource();
			ds.setServerName("STA7400544");
			ds.setPortNumber(1433);
			ds.setDatabaseName("SDBM");
			ds.setIntegratedSecurity(false);
			ds.setUser("java_user");
			ds.setPassword("1234");
			connexion = ds.getConnection ();

            /**********************************************************************/
            /*----------------------------- PC-HOME ------------------------------*/
            /**********************************************************************/

//            SQLServerDataSource ds = new SQLServerDataSource();
//            ds.setServerName("DLX");
//            ds.setPortNumber(1433);
//            ds.setDatabaseName("SDBM");
//            ds.setIntegratedSecurity(false);
//            ds.setUser("java_user");
//            ds.setPassword("1234");
//            connexion = ds.getConnection();

            /**********************************************************************/
            /*----------------------------- PC-MIMIE -----------------------------*/
            /**********************************************************************/

//			SQLServerDataSource ds = new SQLServerDataSource();
//			ds.setServerName("DESKTOP-7MG3U06");
//			ds.setPortNumber(1433);
//			ds.setDatabaseName("SDBM");
//			ds.setIntegratedSecurity(false);
//			ds.setUser("java_user");
//			ds.setPassword("1234");
//			connexion = ds.getConnection();
        }

        // Handle any errors that may have occurred.
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static synchronized Connection getInstance() {
        if (connexion == null)
            new SDBMConnect();
        return connexion;
    }

}