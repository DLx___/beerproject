package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import job.Continent;
import job.Fabricant;
import job.Marque;
import job.Pays;

public class MarqueDAO extends DAO<Marque>
{
	public MarqueDAO(Connection connexion)
	{
		super(connexion);
	}

	@Override
	public Marque getById(int id)
	{
		ResultSet rs;
		Marque marque = null;

		try
		{
			Statement stmt = connexion.createStatement();
			String cmd = "SELECT * FROM MARQUE AS M LEFT JOIN PAYS AS P ON P.ID_PAYS=M.ID_PAYS LEFT JOIN FABRICANT ON FABRICANT.ID_FABRICANT = M.ID_FABRICANT LEFT JOIN CONTINENT AS C ON C.ID_CONTINENT = P.ID_CONTINENT ORDER BY MARQUE.NOM_MARQUE WHERE M.ID_MARQUE = "
					+ id;
			rs = stmt.executeQuery(cmd);
			marque = dataGetMarques(rs);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return marque;
	}

	@Override
	public ArrayList<Marque> getAll()
	{
		ResultSet rs;
		ArrayList<Marque> liste = new ArrayList<>();

		try
		{
			Statement stmt = connexion.createStatement();
			String strCmd = "SELECT * FROM MARQUE LEFT JOIN PAYS AS P ON P.ID_PAYS=MARQUE.ID_PAYS LEFT JOIN FABRICANT ON FABRICANT.ID_FABRICANT = MARQUE.ID_FABRICANT LEFT JOIN CONTINENT AS C ON C.ID_CONTINENT = P.ID_CONTINENT ORDER BY MARQUE.NOM_MARQUE";
			rs = stmt.executeQuery(strCmd);
			
			while (rs.next())
			{
				liste.add(dataGetMarques(rs));
			}

		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return liste;
	}

	private Marque dataGetMarques(ResultSet rs) throws SQLException
	{
		Marque marque;
		marque = new Marque(rs.getInt(1), rs.getString(2),
				new Pays(rs.getInt(5), rs.getString(6), new Continent(rs.getInt(10), rs.getString(11))),
				new Fabricant(rs.getInt(8), rs.getString(9)));
		return marque;
	}

	@Override
	public boolean insert(Marque objet)
	{
		boolean isOk = false;

		try
		{
			Statement stmt = connexion.createStatement();
			String cmd = "INSERT INTO MARQUE (ID_MARQUE,NOM_MARQUE) VALUES (" + objet.getId() + "," + objet.getLibelle()
					+ ")";
			isOk = stmt.executeQuery(cmd) != null;
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return isOk;
	}

	@Override
	public boolean update(Marque objet)
	{
		boolean isOk = false;

		try
		{
			Statement stmt = connexion.createStatement();
			String cmd = "UPDATE FROM MARQUE SET VALUES (" + objet.getId() + "," + objet.getLibelle()
					+ ") WHERE ID_MARQUE = " + objet.getId();
			isOk = stmt.executeQuery(cmd) != null;
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return isOk;
	}

	@Override
	public boolean delete(Marque objet)
	{
		boolean isOk = false;

		try
		{
			Statement stmt = connexion.createStatement();
			String cmd = "DELETE FROM MARQUE WHERE ID_MARQUE = (" + objet.getLibelle() + ")";
			isOk = stmt.executeQuery(cmd) != null;
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		return isOk;
	}

}