package dao;

import java.sql.Connection;

public class DAOFactory
{
	private static final Connection connexion = SDBMConnect.getInstance();

	public static CouleurDAO getCouleurDAO()
	{
		return new CouleurDAO(connexion);
	}

	public static ContinentDAO getContinentDAO()
	{
		return new ContinentDAO(connexion);
	}

	public static PaysDAO getPaysDAO()
	{
		return new PaysDAO(connexion);
	}

	public static FabricantDAO getFabricantDAO()
	{
		return new FabricantDAO(connexion);
	}

	public static MarqueDAO getMarqueDAO()
	{
		return new MarqueDAO(connexion);
	}

	public static TypeBiereDAO getTypeDAO()
	{
		return new TypeBiereDAO(connexion);
	}

	public static ArticleDAO getArticleDAO()
	{
		return new ArticleDAO(connexion);
	}

	public static VenteDAO getVenteDAO()
	{
		return new VenteDAO(connexion);

	}
}
