package view;

import application.Main;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import job.Article;
import job.Couleur;
import job.Marque;
import job.TypeBiere;
import services.ArticlesBean;

import java.math.BigDecimal;
import java.util.Optional;

public class ModificationArticleController
{

    @FXML
    private JFXTextField textFieldLibelle;
    @FXML
    private JFXTextField textFieldPrix;
    @FXML
    private JFXTextField textFieldTitrage;

    @FXML
    private JFXButton annulerButton;
    @FXML
    private JFXButton validerButton;

    @FXML
    private ComboBox<Marque> comboBoxMarque;
    @FXML
    private ComboBox<String> comboBoxVolume;
    @FXML
    private ComboBox<TypeBiere> comboBoxType;
    @FXML
    private ComboBox<Couleur> comboBoxCouleur;

    @FXML
    private Label continentLabel;
    @FXML
    private Label paysLabel;
    @FXML
    private Label fabricantLabel;

    private Main main;
    private ArticlesBean articlesBean;
    private Stage dialogStage;
    private Article articleSelected;

    @FXML
    private void initialize()
    {
    }

    public void setMain(Main main)
    {

        this.main = main;
        articlesBean = this.main.getArticlesBean();
        articleSelected = articlesBean.getArticleSelected();

        textFieldLibelle.setText(articleSelected.getLibelle());
        textFieldPrix.setText(articleSelected.getPrix().toString());
        textFieldTitrage.setText(String.valueOf(articleSelected.getTitrage()));

        continentLabel.setText(articleSelected.getMarque().getPays().getContinent().toString());
        paysLabel.setText(articleSelected.getMarque().getPays().toString());
        fabricantLabel.setText(articleSelected.getMarque().getFabricant().toString());

        comboBoxMarque.setItems(articlesBean.getMarques());
        comboBoxMarque.getSelectionModel().select(articleSelected.getMarque());

        comboBoxType.setItems(articlesBean.getTypes());
        comboBoxType.getSelectionModel().select(articleSelected.getType());

        comboBoxCouleur.setItems(articlesBean.getCouleurs());
        comboBoxCouleur.getSelectionModel().select(articleSelected.getCouleur());

        comboBoxVolume.setItems(FXCollections.observableArrayList("33", "75"));
        if (articleSelected.getVolume() == 33)
        {
            comboBoxVolume.getSelectionModel().select(0);
        }
        if (articleSelected.getVolume() == 75)
        {
            comboBoxVolume.getSelectionModel().select(1);
        }

        System.out.printf("Libell� = %s%nPrix = %s �%nTitrage = %s �%nVolume = %s cl%nCouleur = %s%nMarque = %s%nType = %s", textFieldLibelle.getText(), textFieldPrix.getText(), textFieldTitrage.getText(), comboBoxVolume.getValue(), comboBoxCouleur.getValue(), comboBoxMarque.getValue(), comboBoxType.getValue());

        comboBoxMarque.getSelectionModel().selectedItemProperty().addListener((obersable, oldValues, newValues) -> cbCheckValues(newValues));
    }

    private void cbCheckValues(Marque newValue)
    {
        continentLabel.setText(newValue.getPays().getContinent().getLibelle());
        paysLabel.setText(newValue.getPays().getLibelle());
        fabricantLabel.setText(newValue.getFabricant().getLibelle());
    }

    public void setMenuApplication(Main main)
    {
        this.main = main;
    }

    @FXML
    private void valider()
    {

        articleSelected = articlesBean.getArticleSelected();

        if (textFieldLibelle.getText().equals("") || comboBoxMarque.getValue() == null)
        {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Erreur de saisie");
            alert.setContentText("Veuillez compl�ter l'ensemble des champs de saisie");
            alert.showAndWait();
        }
        else
        {
            Alert alert = new Alert(AlertType.CONFIRMATION);
            alert.getDialogPane().getStylesheets().add("/style.css");
            alert.setTitle("Confirmation Dialog");
            alert.setHeaderText("�tes-vous sur de vouloir modifier l'article avec les param�tres suivants ?");
            String alerteString = String.format("Libell� = %s%nPrix = %s �%nTitrage = %s �%nVolume = %s cl%nCouleur = %s%nMarque = %s%nType = %s", textFieldLibelle.getText(), textFieldPrix.getText(), textFieldTitrage.getText(), comboBoxVolume.getValue(), comboBoxCouleur.getValue(), comboBoxMarque.getValue(), comboBoxType.getValue());
            alert.setContentText(alerteString);

            ButtonType confirmButton = new ButtonType("Confirmer");
            ButtonType cancelButton = new ButtonType("Annuler");
            alert.getButtonTypes().setAll(confirmButton, cancelButton);

            Optional<ButtonType> resultOptional = alert.showAndWait();

            if (resultOptional.get() == confirmButton)
            {
                articleSelected.setLibelle(textFieldLibelle.getText());
                articleSelected.setPrix(new BigDecimal(textFieldPrix.getText()));
                articleSelected.setTitrage(Float.parseFloat(textFieldTitrage.getText()));
                articleSelected.setCouleur(comboBoxCouleur.getValue());
                articleSelected.setVolume(Integer.parseInt(comboBoxVolume.getValue()));
                articleSelected.setType(comboBoxType.getValue());
                articleSelected.setMarque(comboBoxMarque.getValue());
                System.out.println("Modification article : " + articleSelected.toString());
                articlesBean.updateArticle();
                alert.close();
                dialogStage.close();
            }
            else if (resultOptional.get() == cancelButton)
            {
                alert.close();
            }

//				articlesBean.updateArticle();
//				dialogStage.close();
        }

    }

    @FXML
    private void annuler()
    {
        dialogStage.close();
    }

    public Stage getDialogStage()
    {
        return dialogStage;
    }

    public void setDialogStage(Stage dialogStage)
    {
        this.dialogStage = dialogStage;
    }

}
