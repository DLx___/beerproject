package view;

import application.Main;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXSlider;
import com.jfoenix.controls.JFXTextField;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.StackedBarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.stage.Stage;
import job.*;
import services.ArticlesBean;

import java.math.BigDecimal;
import java.util.Optional;

public class MenuController
{
    private static final String SHOW_ALL = "Afficher tous";
    @FXML
    private JFXTextField textFieldSearch;
    @FXML
    private JFXSlider sliderMin;
    @FXML
    private JFXSlider sliderMax;
    @FXML
    private ComboBox<Continent> comboBoxContinent;
    @FXML
    private ComboBox<Pays> comboBoxPays;
    @FXML
    private ComboBox<Marque> comboBoxMarque;
    @FXML
    private ComboBox<Fabricant> comboBoxFabricant;
    @FXML
    private ComboBox<TypeBiere> comboBoxType;
    @FXML
    private ComboBox<Couleur> comboBoxCouleur;
    @FXML
    private ComboBox<String> comboBoxVolume;
    @FXML
    private StackedBarChart<String, Number> stackedBarChartVente;
    @FXML
    private CategoryAxis categorieAxisAnnee;
    @FXML
    private NumberAxis numberAxisQuantite;
    @FXML
    private TableView<Article> tableView;
    @FXML
    private TableColumn<Article, String> nameColumn;
    @FXML
    private TableColumn<Article, Integer> volumeColumn;
    @FXML
    private TableColumn<Article, Float> titrageColumn;
    @FXML
    private TableColumn<Article, Couleur> couleurColumn;
    @FXML
    private TableColumn<Article, TypeBiere> typeColumn;
    @FXML
    private TableColumn<Article, Marque> marqueColumn;
    @FXML
    private TableColumn<Article, Continent> continentColumn;
    @FXML
    private TableColumn<Article, Pays> paysColumn;
    @FXML
    private TableColumn<Article, BigDecimal> prixColumn;
    @FXML
    private TableColumn<Article, Fabricant> fabricantColumn;
    @FXML
    private ButtonBar buttonBarEditDel;
    @FXML
    private JFXButton itemAjouter;
    @FXML
    private JFXButton itemModifier;
    @FXML
    private JFXButton itemSupprimer;
    @FXML
    private JFXButton itemPrint;
    @FXML
    private Button reinitialiserButton;
    @FXML
    private Label counter;
    private Main main;
    private ArticlesBean articlesBean;
    private Stage dialogStage;

    @FXML
    private void initialize()
    {
        articlesBean = new ArticlesBean();

        buttonBarEditDel.setDisable(true);
        stackedBarChartItems();
        adjustSlider();

        comboBoxContinent.setItems(articlesBean.getContinentsFiltrer());
        comboBoxContinent.getSelectionModel().select(0);

        comboBoxCouleur.setItems(articlesBean.getCouleursFiltrer());
        comboBoxCouleur.getSelectionModel().select(0);

        comboBoxPays.setItems(articlesBean.getPaysFiltrer());
        comboBoxPays.getSelectionModel().select(0);

        comboBoxFabricant.setItems(articlesBean.getFabricantsFiltrer());
        comboBoxFabricant.getSelectionModel().select(0);

        comboBoxMarque.setItems(articlesBean.getMarquesFiltrer());
        comboBoxMarque.getSelectionModel().select(0);

        comboBoxType.setItems(articlesBean.getTypesFiltrer());
        comboBoxType.getSelectionModel().select(0);

        comboBoxVolume.setItems(FXCollections.observableArrayList(SHOW_ALL, "33", "75"));
        comboBoxVolume.getSelectionModel().select(0);

        tableView.setItems(articlesBean.getArticles());

        counter.setText(String.valueOf(tableViewCounter()));


        nameColumn.setCellValueFactory((CellDataFeatures<Article, String> feature) -> feature.getValue().libelleProperty());
        volumeColumn.setCellValueFactory((CellDataFeatures<Article, Integer> feature) -> feature.getValue().volumeProperty().asObject());
        titrageColumn.setCellValueFactory((CellDataFeatures<Article, Float> feature) -> feature.getValue().titrageProperty().asObject());
        prixColumn.setCellValueFactory((CellDataFeatures<Article, BigDecimal> feature) -> feature.getValue().prixProperty());
        couleurColumn.setCellValueFactory((CellDataFeatures<Article, Couleur> feature) -> feature.getValue().couleurProperty());
        typeColumn.setCellValueFactory((CellDataFeatures<Article, TypeBiere> feature) -> feature.getValue().typeProperty());
        marqueColumn.setCellValueFactory((CellDataFeatures<Article, Marque> feature) -> feature.getValue().marqueProperty());
        continentColumn.setCellValueFactory((CellDataFeatures<Article, Continent> feature) -> feature.getValue().getMarque().getPays().continentProperty());
        paysColumn.setCellValueFactory((CellDataFeatures<Article, Pays> feature) -> feature.getValue().getMarque().paysProperty());
        fabricantColumn.setCellValueFactory((CellDataFeatures<Article, Fabricant> feature) -> feature.getValue().getMarque().fabricantProperty());

        tableView.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, article) -> setDisableButton(article));
        comboBoxContinent.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, continent) -> cbCheckPays(continent));
        comboBoxFabricant.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, fabricant) -> cbCheckMarque(fabricant));

        sliderMin.valueProperty().addListener((observable, oldValue, aDouble) -> sliderRangeMax((Double) aDouble));
        sliderMax.valueProperty().addListener((observable, oldValue, aDouble) -> sliderRangeMin((Double) aDouble));


    }

    private void setDisableButton(Article article)
    {
        buttonBarEditDel.setDisable(false);
        //comboBoxContinent.disabledProperty();
        comboBoxContinent.disableProperty();
        comboBoxPays.disableProperty();


    }

    private void sliderRangeMin(Double aDouble)
    {
        if (sliderMax.getValue() < sliderMin.getValue())
        {
            sliderMin.setValue(aDouble);
        }
    }

    private void sliderRangeMax(Double newValue)
    {
        if (sliderMax.getValue() < sliderMin.getValue())
        {
            sliderMax.setValue(newValue);
        }
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private void stackedBarChartItems()
    {

        XYChart.Series<String, Number> series = new XYChart.Series<>();
        series.setName("Quantité");

        for (Vente vente : articlesBean.getVentes())
        {
            series.getData().add(new XYChart.Data(vente.getAnnee(), vente.getQuantite()));
        }

        stackedBarChartVente.setCategoryGap(20);
        stackedBarChartVente.widthProperty().addListener((observable, oldValues, newValues) -> {
            Platform.runLater(() -> setMaxBarWidth(stackedBarChartVente, categorieAxisAnnee, 20, 35));
        });

        stackedBarChartVente.getData().addAll(series);
    }

    private void cbCheckPays(Continent continent)
    {
        if (!articlesBean.getPaysFiltrer().get(0).equals(comboBoxPays.getItems().get(0)))
        {
            comboBoxPays.getItems().add(0, new Pays(0, SHOW_ALL));

        }
        if (continent.getId() == 0)
        {
            comboBoxPays.setItems(articlesBean.getPaysFiltrer());
        }
        else
        {
            comboBoxPays.setItems(FXCollections.observableArrayList(continent.getListePays()));
            comboBoxPays.getItems().add(0, new Pays(0, SHOW_ALL));
        }

        comboBoxPays.getSelectionModel().select(0);
    }


    private void cbCheckMarque(Fabricant fabricant)
    {
        if (!articlesBean.getMarquesFiltrer().get(0).equals(comboBoxMarque.getItems().get(0)))
        {
            comboBoxMarque.getItems().add(0, new Marque(0, SHOW_ALL));

        }
        if (fabricant.getId() == 0)
        {
            comboBoxMarque.setItems(articlesBean.getMarquesFiltrer());
        }
        else
        {
            comboBoxMarque.setItems(FXCollections.observableArrayList(fabricant.getListeMarque()));
            comboBoxMarque.getItems().add(0, new Marque(0, SHOW_ALL));

        }

        comboBoxMarque.getSelectionModel().select(0);
    }


    @SuppressWarnings({"rawtypes"})
    @FXML
    private void filtrer()
    {

        String libelle = (textFieldSearch.getText() == null) ? "null" : textFieldSearch.getText();
        Double titrageMin = sliderMin.getValue();
        Double titrageMax = sliderMax.getValue();
        String volume = comboBoxVolume.getSelectionModel().getSelectedItem();
        Continent continent = comboBoxContinent.getSelectionModel().getSelectedItem();
        Pays pays = comboBoxPays.getSelectionModel().getSelectedItem();
        Fabricant fabricant = comboBoxFabricant.getSelectionModel().getSelectedItem();
        Marque marque = comboBoxMarque.getSelectionModel().getSelectedItem();
        TypeBiere type = comboBoxType.getSelectionModel().getSelectedItem();
        Couleur couleur = comboBoxCouleur.getSelectionModel().getSelectedItem();
        articlesBean.setArticlesSearch(libelle, volume, titrageMin, titrageMax, marque, couleur, fabricant, type, pays, continent);
        adjustSlider();

//        categorieAxisAnnee.setLabel("Année");
//        numberAxisQuantite.setLabel("Quantité");
//
//        XYChart.Series<String, Number> series = new XYChart.Series<>();
//        series.setName("Quantité");
//
//        for (Vente vente : articlesBean.getVentes())
//        {
//            articlesBean.filtrerArticles();
//            series.getData().add(new XYChart.Data(vente.getAnnee(), vente.getQuantite()));
//        }


        tableView.setItems(articlesBean.filtrerArticles());
        counter.setText(String.valueOf(tableViewCounter()));
    }

    @FXML
    private void reinitialiser()
    {

        textFieldSearch.setText("");
        comboBoxContinent.getSelectionModel().select(0);
        comboBoxCouleur.getSelectionModel().select(0);
        comboBoxPays.getSelectionModel().select(0);
        comboBoxFabricant.getSelectionModel().select(0);
        comboBoxMarque.getSelectionModel().select(0);
        comboBoxType.getSelectionModel().select(0);
        comboBoxVolume.getSelectionModel().select(0);
        filtrer();
        adjustSlider();
        sliderMin.setValue(articlesBean.getTitrage().get(0));
        sliderMax.setValue(articlesBean.getTitrage().get(1));
    }

    private void adjustSlider()
    {

        sliderMin.setMin(articlesBean.getTitrage().get(0));
        sliderMin.setMax(articlesBean.getTitrage().get(1));
        sliderMax.setMin(articlesBean.getTitrage().get(0));
        sliderMax.setMax(articlesBean.getTitrage().get(1));

    }

    private void setMaxBarWidth(StackedBarChart<String, Number> barChart, CategoryAxis xAxis, double maxBarWidth, double minCategoryGap)
    {
        double barWidth = 0;
        do
        {
            double catSpace = xAxis.getCategorySpacing();
            double availableBarSpace = catSpace - (barChart.getCategoryGap() + barChart.getCategoryGap());
            barWidth = (availableBarSpace / barChart.getData().size()) - barChart.getCategoryGap();
            if (barWidth > maxBarWidth)
            {
                availableBarSpace = (maxBarWidth + barChart.getCategoryGap()) * barChart.getData().size();
                barChart.setCategoryGap(catSpace - availableBarSpace - barChart.getCategoryGap());
            }
        }
        while (barWidth > maxBarWidth);

        do
        {
            double catSpace = xAxis.getCategorySpacing();
            double avilableBarSpace = catSpace - (minCategoryGap + barChart.getCategoryGap());
            barWidth = Math.min(maxBarWidth, (avilableBarSpace / barChart.getData().size()) - barChart.getCategoryGap());
            avilableBarSpace = (barWidth + barChart.getCategoryGap()) * barChart.getData().size();
            barChart.setCategoryGap(catSpace - avilableBarSpace - barChart.getCategoryGap());
        }
        while (barWidth < maxBarWidth && barChart.getCategoryGap() > minCategoryGap);
    }

    public void setMenuApplication(Main main)
    {
        this.main = main;
    }


    private int tableViewCounter()
    {
        return tableView.getItems().size();
    }

    @FXML
    private void modifier()
    {
        main.getArticlesBean().setArticleSelected(tableView.getSelectionModel().getSelectedItem());
        main.showModifArticle();
        tableView.refresh();
    }

    @FXML
    private void ajouter()
    {
        main.getArticlesBean().setArticleSelected(new Article(0, null, 0, 0, null));
        tableView.getSelectionModel().select(null);
        main.showAjoutArticle();
        tableView.refresh();

    }

    @FXML
    private void supprimer()
    {

        if (main.getArticlesBean().getArticleSelected() != null)
        {

            Alert alert = new Alert(AlertType.CONFIRMATION);

            alert.getDialogPane().getStylesheets().add("/style.css");
            alert.setTitle("Confirmation Dialog");
            alert.setHeaderText("Êtes-vous sur de vouloir supprimer l'article ?");
            String contentText = String.format("Libellé = %s%nVolume = %s%nTitrage = %s%nCouleur = %s%nType = %s%nMarque = %s%nFabricant = %s%nContinent  %s%nPays = %s%nPrix = %s%n", tableView.getSelectionModel().getSelectedItem().getLibelle(), tableView.getSelectionModel().getSelectedItem().getVolume(), tableView.getSelectionModel().getSelectedItem().getTitrage(), tableView.getSelectionModel().getSelectedItem().getCouleur(), tableView.getSelectionModel().getSelectedItem().getType(), tableView.getSelectionModel().getSelectedItem().getMarque(), tableView.getSelectionModel().getSelectedItem().getMarque().getFabricant(), tableView.getSelectionModel().getSelectedItem().getMarque().getPays().getContinent(), tableView.getSelectionModel().getSelectedItem().getMarque().getPays(), tableView.getSelectionModel().getSelectedItem().getPrix());
            alert.setContentText(contentText);

            ButtonType confirmButton = new ButtonType("Confirmer");
            ButtonType cancelButton = new ButtonType("Annuler");

            alert.getButtonTypes().setAll(confirmButton, cancelButton);

            Optional<ButtonType> resultOptional = alert.showAndWait();

            if (resultOptional.get() == confirmButton)
            {
                articlesBean.deleteArticle();
                tableView.refresh();
                alert.close();
            }
            else if (resultOptional.get() == cancelButton)
            {
                alert.close();
            }
        }

    }
}