package application;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import services.ArticlesBean;
import view.AjouterArticleController;
import view.MenuController;
import view.ModificationArticleController;

public class Main extends Application
    {
    private Stage primaryStage;
    private AnchorPane root;
    private ArticlesBean articlesBean;
    private AnchorPane anchorPane;

    public static void main(String[] args)
    {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage)
    {

        articlesBean = new ArticlesBean();

        try
        {
            this.primaryStage = primaryStage;
            root = new AnchorPane();
            primaryStage.setTitle("Beer management");
            primaryStage.show();
            showController();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void showController()
    {
        try
        {
            FXMLLoader myFXMLloader = new FXMLLoader();
            myFXMLloader.setLocation(Main.class.getResource("/controller.fxml"));
            root = myFXMLloader.load();

            Scene scene = new Scene(root, 1207, 800);
            scene.getStylesheets().add(getClass().getResource("/style.css").toExternalForm());
            primaryStage.setResizable(false);
            primaryStage.setScene(scene);
            MenuController menuController = myFXMLloader.getController();
            menuController.setMenuApplication(this);

        }
        catch (Exception e)
        {
            e.printStackTrace();
            // System.out.println(e.getMessage());


        }
    }

    public void showModifArticle()
    {
        try
        {
            FXMLLoader myFXMLloader = new FXMLLoader();
            myFXMLloader.setLocation(Main.class.getResource("/modificationArticle.fxml"));
            AnchorPane anchorPane = myFXMLloader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Modifier un article");
            // dialogStage.getIcons().add(new
            // Image(this.getClass().getResourceAsStream("../resource/addSalarie.png")));
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);

            Scene scene = new Scene(anchorPane);
            scene.getStylesheets().add(getClass().getResource("/style.css").toExternalForm());
            dialogStage.setScene(scene);
            ModificationArticleController modificationArticleController = myFXMLloader.getController();
            modificationArticleController.setDialogStage(dialogStage);
            modificationArticleController.setMain(this);

            dialogStage.showAndWait();

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void showAjoutArticle()
    {
        try
        {
            FXMLLoader myFXMLloader = new FXMLLoader();
            myFXMLloader.setLocation(Main.class.getResource("/ajouterArticle.fxml"));
            AnchorPane anchorPane = myFXMLloader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Ajouter un article");
            // dialogStage.getIcons().add(new
            // Image(this.getClass().getResourceAsStream("../resource/addSalarie.png")));
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);

            Scene scene = new Scene(anchorPane);
            scene.getStylesheets().add(getClass().getResource("/style.css").toExternalForm());
            dialogStage.setScene(scene);
            AjouterArticleController ajouterArticleController = myFXMLloader.getController();
            ajouterArticleController.setDialogStage(dialogStage);
            ajouterArticleController.setMain(this);

            dialogStage.showAndWait();

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public ArticlesBean getArticlesBean()
    {
        return articlesBean;
    }

    public void setArticlesBean(ArticlesBean articlesBean)
    {
        this.articlesBean = articlesBean;
    }

    }
